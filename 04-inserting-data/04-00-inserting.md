# Interting Data

Even that DeepBlue contains the experiments from  [ENCODE](https://www.genome.gov/encode/) and [BluePrint project](http://www.blueprint-epigenome.eu/), and controlled vocabulary terms from [ENCODE controlled vocabulary](ftp://hgdownload.cse.ucsc.edu/apache/cgi-bin/encode/cv.ra), [Cell type Ontology](http://www.ontobee.org/browser/index.php?o=CL), [Experimental Factor Ontology](http://www.ontobee.org/browser/index.php?o=EFO), and [Uber anatomy ontology]: http://www.ontobee.org/browser/index.php?o=UBERON, the users may need to insert their own terms in the controlled vocabularies, or insert new annotations and experiments data.

***All inserted data: vocabulary terms, annotations, and experiments will be available for all users after the insertion.***
