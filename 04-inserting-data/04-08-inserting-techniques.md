## Inserting Technique

The [add_technique](http://deepblue.mpi-inf.mpg.de/api.html#api-add_technique) requires the  ```name``` and ```description```.


```python
(status, p_id) = server.add_technique("SOLiD ", "Sequencing by ligation", user_key)
```