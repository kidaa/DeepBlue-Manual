# Data types

The main DeepBlue data type is the *region*. An experiment or an annotation contains a region set.
For describing the [experiments](02-01-experiments.md) and [annotations](02-02-annotations.md), they contain metadata.
Metadata is the description of the data, in our case, the experiment and annotation.
The experiments metadata contains: name of the experiment, *genome*, *epigenetic mark*, *sample*, *technique*, and *project*.
These metadata fields are organized into [controlled vocabularies](02-03-controlled-vocabulary.md).