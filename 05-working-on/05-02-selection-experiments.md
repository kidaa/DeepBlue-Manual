## Selection Experiments

To [select_regions](http://deepblue.mpi-inf.mpg.de/api.html#api-select_regions) is used to select the experiments data. The [select_regions](http://deepblue.mpi-inf.mpg.de/api.html#api-select_regions) is similar to [select_annotations](http://deepblue.mpi-inf.mpg.de/api.html#api-select_annotations). The difference is that [select_regions](http://deepblue.mpi-inf.mpg.de/api.html#api-select_regions) accepts ```epigenetic_mark```, ```sample```, ```technique```, and ```project```.
