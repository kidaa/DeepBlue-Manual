## Counting Regions

[count_regions](http://deepblue.mpi-inf.mpg.de/api.html#api-count_regions) is the simplest operation that can be made on a query.
It return the number of regions that a query has.

```python
(status, count) = server.count_regions(query_id, user_key)
```	 

 ```python
['okay', 1338676]
```

