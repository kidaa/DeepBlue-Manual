![DeepBlue Epigenomic Data Server](http://deepblue.mpi-inf.mpg.de/imgs/deepblue_alt.png)

# DeepBlue Epigenomic Data Server Manual - DRAFT

This manual presents DeepBlue, its commands, usage examples and advices.


## Reference Guide
A list of all commands is available at [DeepBlue API Page](http://deepblue.mpi-inf.mpg.de/api.html). This list is updated automatically when new commands are inserted or modified.


The data stays in the server, downloading only the final result.

## Remarks

DeepBlue is a work in progress. API and data can (and probably will) change.

DeepBlue only import public data available. 

Uploading data to the DeepBlue will make your data accessible for the others users.