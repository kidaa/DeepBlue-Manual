# Exploring

DeepBlue currently has more than 4000 experiments. 
The experiments are organized using [controlled vocabularies](../02-data-types/02-03-controlled-vocabulary.md).
Every controlled vocabulary has a [listing](03-01-listing.md) operation, helping the user to find the right names.

Although the [listing](03-01-listing.md) commands provides an easy interface for to the controlled vocabularies, 
they do not provide a search operation.
DeepBlue provides the command [search](http://deepblue.mpi-inf.mpg.de/api.html#api-search) for [free-text searching](03-02-free-text-searching.md).

The [listing](03-01-listing.md) and [free-text searchingg](03-02-free-text-searching.md) commands return a set of id and name.
It is possible to read the [data identifier](03-03-data-identifier.md) using the command [info](http://deepblue.mpi-inf.mpg.de/api.html#api-info).

The next sections will focus on the [listingg](03-01-listing.md), [free-text searching](03-02-free-text-searching.md) , and [data identifier](03-03-data-identifier.md) commands.